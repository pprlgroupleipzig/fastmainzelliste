package de.pseudonymisierung.mainzelliste.blocker;

/**
 * Definition of the strategy for {@link HLsh} blocking on multiple fields
 */
public enum HLshMethod {

	CONCAT("concat"),
	LOCAL_SELECTION("localSelection"),
	GLOBAL_SELECTION("globalSelection");

	private String name;

	HLshMethod(String name) {
		this.name = name;
	}

	public static HLshMethod from(String s) {
		for (final HLshMethod hLshMethod : HLshMethod.values()) {
			if (hLshMethod.name.equalsIgnoreCase(s)) {
				return hLshMethod;
			}
		}
		throw new IllegalArgumentException("No constant " + s + " found");
	}
}
